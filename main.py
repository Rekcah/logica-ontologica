import numpy as np
import configparser
import re
import logging

########## CONFIG ##########
config = configparser.ConfigParser()
config.read('config.ini')
statistics = config.get('DEBUG', 'statistics')
left_bracket = config.get('SYMBOLS', 'left_bracket')
right_bracket = config.get('SYMBOLS', 'right_bracket')
not_symbol = config.get('SYMBOLS', 'not_symbol')
and_symbol = config.get('SYMBOLS', 'and_symbol')
or_symbol = config.get('SYMBOLS', 'or_symbol')
xor_symbol = config.get('SYMBOLS', 'xor_symbol')
implication_symbol = config.get('SYMBOLS', 'implication_symbol')
coimplication_symbol = config.get('SYMBOLS', 'coimplication_symbol')

#Logical values
logical_values = ['V', 'F', 'N']

#Symbols set needed in Process.elaborate_in_brackets
symbols_set = [not_symbol, and_symbol, or_symbol, xor_symbol, implication_symbol, coimplication_symbol]


########## CORE ##########
class LogicaOntologica:
    def __init__(self, input_string, statistics):
        self.statistics = statistics

        ternary_list = []
        for letter in input_string:
            if letter in ["V", "F", "N", "v", "f", "n"]:
                ternary_list.append([letter.upper()]*3)
            else:
                ternary_list.append(['V', 'F', 'N'])
        ternary_matrix = np.array(ternary_list)
        combination_list = self.generate_combinations(ternary_matrix)

        # Avoid repetitions on input generic variables
        variable_values = {}
        total_variable_values = []
        for combination in combination_list:
            for n, letter in enumerate(input_string):
                variable_values[f"{letter}{n}"] = combination[n]
            total_variable_values.append(str(variable_values))

        final_duplicates_combination_list = []
        for variable_values_element in total_variable_values:
            keys = {}
            variable_values_element_done = eval(variable_values_element).copy()
            for key in eval(variable_values_element).keys():
                # Check if variable is repeated in expression
                if key[:-1] in keys:
                    variable_values_element_done[key] = keys[(key[:-1])]
                else:
                    keys[(key[:-1])] = eval(variable_values_element)[key]

            final_duplicates_combination_list.append(variable_values_element_done)

        final_combination_list = [dict(t) for t in set([tuple(d.items()) for d in final_duplicates_combination_list])]

        if isinstance(self.statistics, str) and self.statistics.lower() == "true":
            print("")
            print(f"Number of variables involved: {len(final_combination_list[0])}")
            print(f"Number of combinations obtained: {len(final_combination_list)}")
            self.separate_text()

        self.final_combination_list = final_combination_list

    #Generate text to separate raw's text
    def separate_text(self):
        separate = "-"*30+"\n"
        print(separate)

    def generate_combinations(self, arrays, out=None):
        arrays = [np.asarray(x) for x in arrays]
        dtype = arrays[0].dtype

        n = np.prod([x.size for x in arrays])
        if out is None:
            out = np.zeros([n, len(arrays)], dtype=dtype)

        m = int(n / arrays[0].size)
        out[:,0] = np.repeat(arrays[0], m)
        if arrays[1:]:
            self.generate_combinations(arrays[1:], out=out[0:m, 1:])
            for j in range(1, arrays[0].size):
                out[j*m:(j+1)*m, 1:] = out[0:m, 1:]
        return out
        

class Processing:
    def calculate(self, input_string):
        invalid_expression = False

        #Check that the truth values are not close to each other
        for i in range(len(input_string)-1):
            if input_string[i] in logical_values and input_string[i+1] in logical_values:
                invalid_expression = True

        #Check if truth values are followed by "("
        for i in range(len(input_string)-1):
            if input_string[i] in logical_values and input_string[i+1] == "(":
                invalid_expression = True

        #Checking if truth values are preceded by ")"
        for i in range(1, len(input_string)):
            if input_string[i] in logical_values and input_string[i-1] == ")":
                invalid_expression = True

        #Check if there is a succession of symbols
        symbols_set_without_negation = symbols_set[:]
        symbols_set_without_negation.remove(not_symbol)
        for i in range(len(input_string) - 1):
            if input_string[i] in symbols_set_without_negation and input_string[i+1] in symbols_set_without_negation:
                invalid_expression = True
        if invalid_expression is True:
            logging.error("Invalid expression on " + input_string)
            exit(1)
        while bool(self.match_BRACKETS(self, input_string)) is True:
            input_string = self.elaborate_with_brackets(self, input_string)
        return self.process_one_string(self,input_string)

    def elaborate_with_brackets(self, input_string):
        invalid_brackets = "False" if len(input_string) % 2 == 0 else "True"
        if invalid_brackets is True:
            logging.error("The number of brackets is not even.")
            exit(1)
        number_of_brackets = (input_string.count("("), input_string.count(")"))
        if number_of_brackets[0] != number_of_brackets[1]:
            logging.error("A parenthesis remains open.")
            exit(1)
        first_value_inside_brackets = re.search(r"\(([^()]+)\)", input_string)
        bracket_result = self.process_one_string(self,first_value_inside_brackets.group(1))
        output_string = input_string[:first_value_inside_brackets.span()[0]] + bracket_result + input_string[first_value_inside_brackets.span()[1]:]
        return output_string

    def process_one_string(self, input_string):
        while len(input_string) > 1 and any(character in logical_values for character in input_string) and any(character in symbols_set for character in input_string):
            if bool(self.match_BRACKETS(self, input_string)) is True:
                output_string = ''.join(c for c in input_string if c not in ['(', ')'])
                return output_string
            input_string = self.logical_process(self, input_string)
        if len(input_string) == 1 and input_string in logical_values:
            output_string = ''.join(c for c in input_string if c not in ['(', ')'])
            return output_string
        elif len(input_string) > 1 and any(character in logical_values for character in input_string) and bool(self.match_BRACKETS(self, input_string)) is True:
            output_string = ''.join(c for c in input_string if c not in ['(', ')'])
            return output_string
        else:
                logging.error("No logical values were found in the specified inference: " + input_string)
                exit(1)
    
    def logical_process(self, input_string):
        if bool(re.findall('(?<=' + not_symbol + ').', input_string)) is True:
            return self.calculate_NOT(self, input_string)
        symbols_set_regex = r'[' + re.escape(''.join(symbols_set)) + ']'
        match_AND_OR_XOR = re.search(symbols_set_regex, input_string)
        if bool(match_AND_OR_XOR) is True:
            if match_AND_OR_XOR.group(0) == and_symbol:
                return self.calculate_AND(self, input_string)
            elif match_AND_OR_XOR.group(0) == or_symbol:
                return  self.calculate_OR(self, input_string)
            elif match_AND_OR_XOR.group(0) == xor_symbol:
                return self.calculate_XOR(self, input_string)
        match_IMPLICATION_COIMPLICATION = re.search(symbols_set_regex, input_string)
        if bool(match_IMPLICATION_COIMPLICATION) is True:
            if match_IMPLICATION_COIMPLICATION.group(0) == implication_symbol:
                return self.calculate_IMPLICATION(self, input_string)
            elif match_IMPLICATION_COIMPLICATION.group(0) == coimplication_symbol:
                return self.calculate_COIMPLICATION(self, input_string)

    ###Logical operations###
    def calculate_NOT(self, input_string):
        match_first_negation = re.search("!+[^!]", input_string)
        if bool(match_first_negation) is True:
            match_first_negation_string = match_first_negation.group(0)
            value_to_negate = match_first_negation_string[-1]
            if value_to_negate not in logical_values:
                logging.error("Invalid operation on " + input_string + "→ " + match_first_negation_string)
                exit(1)
            number_of_negations = len(match_first_negation_string.replace(value_to_negate, ""))
            if number_of_negations % 2 == 0:
                if value_to_negate == "V" or value_to_negate == "N":
                    result = "N"
                elif value_to_negate == "F":
                    result = "F"
            else:
                if value_to_negate == "V" or value_to_negate == "N":
                    result = "F"
                elif value_to_negate == "F":
                    result = "N"
            output_string = input_string.replace(input_string[match_first_negation.span()[0]:match_first_negation.span()[1]], result, 1)
            return output_string

    def calculate_AND(self, input_string):
        match_AND = r'([A-Z])' + re.escape(and_symbol) + '([A-Z])'
        match = re.search(match_AND, input_string)
        logical_results = {
            'VV': 'V',
            'VF': 'F',
            'VN': 'F',
            'FV': 'F',
            'FF': 'F',
            'FN': 'N',
            'NV': 'F',
            'NF': 'N',
            'NN': 'F'
            }
        result = logical_results[match.group(1) + match.group(2)]
        input_string = re.sub(match_AND, result, input_string)
        return input_string

    def calculate_OR(self, input_string):
        match_AND = r'([A-Z])' + re.escape(or_symbol) + '([A-Z])'
        match = re.search(match_AND, input_string)
        logical_results = {
            'VV': 'V',
            'VF': 'V',
            'VN': 'V',
            'FV': 'V',
            'FF': 'F',
            'FN': 'F',
            'NV': 'V',
            'NF': 'F',
            'NN': 'N'
            }
        result = logical_results[match.group(1) + match.group(2)]
        input_string = re.sub(match_AND, result, input_string)
        return input_string

    def calculate_XOR(self, input_string):
        match_AND = r'([A-Z])' + re.escape(xor_symbol) + '([A-Z])'
        match = re.search(match_AND, input_string)
        logical_results = {
            'VV': 'F',
            'VF': 'F',
            'VN': 'F',
            'FV': 'F',
            'FF': 'N',
            'FN': 'F',
            'NV': 'F',
            'NF': 'F',
            'NN': 'N'
            }
        result = logical_results[match.group(1) + match.group(2)]
        input_string = re.sub(match_AND, result, input_string)
        return input_string

    def calculate_IMPLICATION(self, input_string):
        match_AND = r'([A-Z])' + re.escape(implication_symbol) + '([A-Z])'
        match = re.search(match_AND, input_string)
        logical_results = {
            'VV': 'V',
            'VF': 'F',
            'VN': 'F',
            'FV': 'V',
            'FF': 'V',
            'FN': 'V',
            'NV': 'V',
            'NF': 'V',
            'NN': 'V'
            }
        result = logical_results[match.group(1) + match.group(2)]
        input_string = re.sub(match_AND, result, input_string)
        return input_string

    def calculate_COIMPLICATION(self, input_string):
        match_AND = r'([A-Z])' + re.escape(coimplication_symbol) + '([A-Z])'
        match = re.search(match_AND, input_string)
        logical_results = {
            'VV': 'V',
            'VF': 'F',
            'VN': 'N',
            'FV': 'F',
            'FF': 'F',
            'FN': 'N',
            'NV': 'N',
            'NF': 'N',
            'NN': 'N'
            }
        result = logical_results[match.group(1) + match.group(2)]
        input_string = re.sub(match_AND, result, input_string)
        return input_string

    def match_BRACKETS(self, input_string):
        return re.search('(\(([^()]|)*\))', input_string)

print("Logica ontologica")
input_string = input("> : ")
input_string = input_string.replace(" ", "")

#Check if input_string is void
if len(input_string.strip()) == 0:
    logging.error("Cannot process an empty expression.")
    exit(1)

#Delete all symbols from input_string
expression_without_symbols = ''.join(char for char in input_string if char not in symbols_set and char not in ['(', ')'])
#Calculate all permutations
values_combinations = LogicaOntologica(expression_without_symbols, statistics).final_combination_list
#Map logical values on permutations
values_combinations_list = []
for value_combination in values_combinations:
    values_combinations_list.append(list(value_combination.values()))

logical_operations = []
for combination in values_combinations_list:
    mapping_index = 0
    string_to_be_mapped = input_string
    for index, char in enumerate(input_string):
        if char not in symbols_set and char not in ['(', ')']:
            string_to_be_mapped = string_to_be_mapped[:index] + combination[mapping_index] + string_to_be_mapped[index+1:]
            mapping_index += 1
    logical_operations.append(string_to_be_mapped)
logical_operations.sort(reverse=True)

results = []
#Calculate logical operations
for logical_operation in logical_operations:
    result = Processing.calculate(Processing, logical_operation)
    print(logical_operation, '=', result)
    results.append(result)

#Check if the expression is a tautology
if all(element == results[0] for element in results) is True and len(results) > 1:
    print("The expression is a tautology!")